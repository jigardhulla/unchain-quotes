@include('layouts.header')
    <div class="container mx-auto">
        @yield('content')
    </div>
@include('layouts.footer')
