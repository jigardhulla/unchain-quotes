        <footer class="flex text-gray-500 text-center text-sm py-4">
            <div class="flex-1"><a target="_blank" href="https://www.twitter.com/{{config('unquote.screen_name')}}"><svg class="fill-current inline-block" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z"/></svg></div>
            <div class="flex-1">
                <a class="hover:text-blue-500" href="{{url('/tos')}}">ToS</a> 
                | <a class="hover:text-blue-500" href="{{url('/privacy_policy')}}">Privacy Policy</a>
                | Version: {{ config('app.version') }}
                </div>
            <div class="flex-1">Made with
                <svg class="h-4 w-4 inline-block fill-current text-red-700" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 3.22l-.61-.6a5.5 5.5 0 0 0-7.78 7.77L10 18.78l8.39-8.4a5.5 5.5 0 0 0-7.78-7.77l-.61.61z"/></svg>
                by <a class="text-blue-700 hover:text-blue-300" target="_blank" href="https://www.twitter.com/jigar_dhulla">{{'@'}}jigar_dhulla</a>
            </div>
        </footer>
    </body>
    <script>
    document.addEventListener("DOMContentLoaded", function(){
        document.getElementById('menu-button').onclick = function(){
            var menuItems = document.getElementById('menu-items');
            if (menuItems.classList.contains('sm:hidden')) {
                // The box that we clicked has a class of bad so let's remove it and add the good class
                menuItems.classList.remove('sm:hidden');
            }else{
                menuItems.classList.add('sm:hidden');
            }
            if (menuItems.classList.contains('hidden')) {
                // The box that we clicked has a class of bad so let's remove it and add the good class
                menuItems.classList.remove('hidden');
            }else{
                menuItems.classList.add('hidden');
            }
        }
    });
    </script>
</html>
