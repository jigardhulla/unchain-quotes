<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') - {{ config('app.name') }}</title>
    <title>Unquote Tweets</title>
    {{-- <script src="https://unpkg.com/@webcomponents/webcomponentsjs@2/bundles/webcomponents-sd-ce.js"></script> --}}
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.min.css') }} "> <!--load all styles -->
    <link rel="shortcut icon" href="{{url('/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{url('/favicon.ico')}}" type="image/x-icon">
    <!-- Start Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-161715352-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-161715352-1');
    </script>
    <!-- End Global site tag (gtag.js) - Google Analytics -->
</head>
<body class="flex flex-col bg-gray-200">
    <nav class="flex items-center justify-between flex-wrap bg-blue-500 p-6">
        <a href="{{url('/')}}">
            <div class="flex items-center flex-shrink-0 text-white mr-6">
                    <img class="w-8 h-8 mr-2" src="{{asset('/images/logo.png')}}" alt="Logo">
                    {{-- <svg class="fill-current h-8 w-8 mr-2 inline-block" width="54" height="54" viewBox="0 0 54 54" xmlns="http://www.w3.org/2000/svg"><path d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z"/></svg> --}}
                    <span class="font-semibold text-xl tracking-tight">{{ config('app.name') }}</span>
            </div>
        </a>

        <div id="menu-button" class="block lg:hidden">
            <button class="flex items-center px-3 py-2 border rounded text-blue-200 border-blue-400 hover:text-white hover:border-white">
            <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
            </button>
        </div>
        <div id="menu-items" class="w-full hidden sm:hidden block flex-grow lg:flex lg:items-center lg:w-auto">
            <div class="text-sm lg:flex-grow">
                <a href="{{url('/about')}}" class="block mt-4 lg:inline-block lg:mt-0 text-blue-200 hover:text-white mr-4">
                        About
                    </a>
                {{-- <a href="{{url('/pricing')}}" class="block mt-4 lg:inline-block lg:mt-0 text-blue-200 hover:text-white mr-4">
                        Pricing
                </a> --}}
                <a href="{{url('/help')}}" class="block mt-4 lg:inline-block lg:mt-0 text-blue-200 hover:text-white">
                    Help
                </a>
            </div>
            {{-- <div>
                <a href="#" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue-500 hover:bg-white mt-4 lg:mt-0">Download</a>
            </div> --}}
        </div>
    </nav>
