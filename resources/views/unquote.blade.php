@extends('layouts.app')

@section('title', 'Unquoted Tweet')

@section('content')
    <div class="my-12">
        @foreach($tweets as $i => $tweet)
        <twitter-status class="border-2 border-blue-400" status="{{ $tweet['full_tweet'] }}"></twitter-status>
        @if($i != ($tweetCount - 1))
            <div class="mx-auto h-5 w-1 bg-gray-600"> </div>
        @endif
        @endforeach
    </div>
    <script async src="https://unpkg.com/twitter-status@0.4.1/dist/twitter-status.min.js"></script>
@endsection
