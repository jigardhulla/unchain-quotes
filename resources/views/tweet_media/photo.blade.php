<div class="grid{{ count($media['photo']) > 1 ? ' grid-cols-2' : '' }} p-8">
    
    @foreach($media['photo'] as $photo)
        {{-- <img src="{{$photo->media_url_https}}" /> --}}
        
        <div class="bg-white rounded h-full text-grey-900 no-underline shadow-md">
            {{-- <h1 class="text-3xl p-6">fox.</h1> --}}
            <img class="w-full h-auto block rounded-b border border-gray-300" src="{{$photo->media_url_https}}" alt="Image">
        </div>
    @endforeach
</div>