@foreach($media['video'] as $video)
    {{-- {{ dd($video) }} --}}
    {{-- <img src="{{$gif->video_info->variants[0]->url}}" /> --}}
    <div>
        <video class="mt-2"  
            controls
            poster="{{$video->media_url_https}}" 
            src="{{$video->video_info->variants[0]->url}}" 
            type="video/mp4">
            Your browser doesn't support videos.
        </video>
    </div>
@endforeach