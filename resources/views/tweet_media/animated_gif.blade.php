@foreach($media['animated_gif'] as $gif)
    {{-- <img src="{{$gif->video_info->variants[0]->url}}" /> --}}
    <div>
        <video autoplay 
            loop 
            controls
            poster="{{$gif->media_url_https}}" 
            src="{{$gif->video_info->variants[0]->url}}" 
            type="video/mp4">
            Your browser doesn't support videos.
        </video>
    </div>
@endforeach