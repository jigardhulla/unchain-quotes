@extends('layouts.app')

@section('title', 'Conversation')

@section('content')
<div class="mx-5 mt-5 lg:mx-auto md:mx-8 md:mt-8 border-2 border-solid rounded-lg bg-white sm:w-11/12 lg:w-8/12">
    @foreach($tweets as $tweet)
    <div class="flex md:flex-row flex-col border-b">
        <!-- Avatar -->
        <div class="md:w-2/12 lg:w-1/12 text-right pl-3 pt-3 items-center">
            {{-- <div><i class="fa fa-thumb-tack text-teal mr-2"></i></div> --}}
            <div>
                <a href="#">
                    <img src="{{ $tweet->tweeter->profile_image_url }}"
                            alt="avatar"
                            class="rounded-full h-12 w-12 mr-2" />
                </a>
            </div>
        </div>
        <!-- Tweet Content -->
        <div class="md:w-10/12 lg:w-11/12 p-3 md:pl-0">
            <!-- Tweet Header -->
            <div class="flex justify-between">
                <div>
                    <span class="font-bold">
                        <a href="#" class="text-black">{{ $tweet->tweeter->name }}</a>
                    </span>
                    <span class="text-grey-dark">{{ '@' . $tweet->tweeter->screen_name }}</span>
                    <span class="text-grey-dark">&middot;</span>
                    <span class="text-grey-dark"><span title="{{ $tweet->tweet_created_at }}">{{ $tweet->tweet_created_at->diffForHumans() }}</span></span>
                </div>
                {{-- <div>
                    <a href="#"
                    class="text-grey-dark hover:text-teal"><i class="fa fa-chevron-down"></i></a>
                </div> --}}
            </div>

            <!-- Tweet Body -->
            <div>
                <div class="flex flex-col mb-4">
                    <p>{{ $tweet->text }}</p>
                    <div class="flex">
                    @if(!empty($media[$tweet->status_id]))
                        @include('tweet_media.' . implode(array_keys($media[$tweet->status_id])), ['media' => $media[$tweet->status_id]])
                    @endif
                    </div>
                </div>
            </div>

            <!-- Buttons -->
            <div class="pb-2">
                <span class="mr-8">
                    <a href="https://twitter.com/intent/tweet?in_reply_to={{$tweet->status_id}}" target="_blank" class="text-gray-600 hover:no-underline hover:text-blue-300">
                        <i class="fa fa-comment fa-lg mr-2"></i>
                    </a>
                </span>
                <span class="mr-8">
                    <a href="https://twitter.com/intent/retweet?tweet_id={{$tweet->status_id}}" target="_blank" class="text-gray-600 hover:no-underline hover:text-green-300">
                        <i class="fa fa-retweet fa-lg mr-2"></i>
                    </a>
                </span>
                <span class="mr-8">
                    <a href="https://twitter.com/intent/like?tweet_id={{$tweet->status_id}}" target="_blank" class="text-gray-600 hover:no-underline hover:text-red-300">
                        <i class="fa fa-heart fa-lg mr-2"></i>
                    </a>
                </span>
                {{-- <span class="mr-8">
                    <a href="#" class="text-gray-600 hover:no-underline hover:text-teal-300">
                        <i class="fa fa-envelope fa-lg mr-2"></i>
                    </a>
                </span> --}}
            </div>
        </div>
    </div>
    @endforeach
</div>
    {{-- <div class="my-12">
        @foreach($tweets as $i => $tweet)
        <twitter-status class="border-2 border-blue-400" status="{{ $tweet['full_tweet'] }}"></twitter-status>
        @if($i != ($tweetCount - 1))
            <div class="mx-auto h-5 w-1 bg-gray-600"> </div>
        @endif
        @endforeach
    </div> --}}
    {{-- <script async src="https://unpkg.com/twitter-status@0.4.1/dist/twitter-status.min.js"></script> --}}
@endsection
