@extends('layouts.app')

@section('title', 'About')

@section('content')
<div><h1 class="text-4xl py-4 pl-4">About</h1></div>
<div class="pb-4"><hr /></div>
<div class="flex flex-col md:flex-row">
    <div class="flex flex-col mb-4">
        <ul class="list-disc border border-2 border-gray-500 rounded-lg mx-4 px-8">
            <li class="py-2"><a href="#about-unchain-quotes">About Unchain Quotes</a></li>
            <li class="py-2"><a href="#about-team">Team</a></li>
            <li class="py-2"><a href="#about-pricing">Pricing</a></li>
            <li class="py-2"><a href="#about-inspiration">Inspiration</a></li>
            <li class="py-2"><a href="#about-credits">Credits</a></li>
            <li class="py-2"><a href="#about-support-us">Support Us</a></li>
        </ul>
    </div>
    <div class="flex flex-col px-4">
        <div class="pb-4" id="about-unchain-quotes">
            <h1 class="text-2xl pb-2">About Unchain Quotes</h1>
            <p>Unchain Quotes is a service that enables everyone to read Chained Quotes(Reply with Comment) in Easy-to-Read webpage.</p>
            <p>This will basically help solve the problem of going through each tweet in the chain.</p>
            <p>Also, non-Twitter users can catchup on conversations that happen on Twitter</p>
        </div>
        <div class="pb-4" id="about-team">
            <h1 class="text-2xl pb-2">Team</h1>
            <div class="flex flex-row">
                <div class="text-center mr-5">
                    <a class="text-blue-700 hover:text-blue-300" href="https://www.twitter.com/jigar_dhulla">
                        <img class="w-32 h-32 rounded-full" title="Jigar Dhulla" src="{{asset('/images/jigar_dhulla.jpg')}}" alt="Jigar Dhulla">
                    </a>
                    <span><a class="text-blue-700 hover:text-blue-300" href="https://www.twitter.com/jigar_dhulla">@jigar_dhulla</a></span>
                </div>
                <div class="text-center">
                    <img class="w-32 h-32 rounded-full" title="Jigar Dhulla" src="{{asset('/images/uiux.jpg')}}" alt="Jigar Dhulla">
                    <span>@You</span>
                </div>
            </div>
        </div>
        <div class="pb-4" id="about-pricing">
            <h1 class="text-2xl pb-2">Pricing</h1>
            <h2 class="text-xl">Free</h2>
            <p>Primary feature to Unchain Quotes will always remain Free!</p>
            <h2 class="text-xl">Premium</h2>
            <p>Please contact us at {{config('unquote.email')}} if you want any feature to be supported</p>
        </div>
        <div class="pb-4" id="about-inspiration">
            <h1 class="text-2xl pb-2">Inspiration</h1>
            <h2 class="text-xl"><a class="text-blue-700 hover:text-blue-300" href="https://threadreaderapp.com/">Thread Reader</a></h2>
            <p>Thread Reader helps you discover great content of an Author.</p>
            <p>Twitter Bot converts the thread of an author into a clean webpage.</p>
        </div>
        <div class="pb-4" id="about-credits">
            <h1 class="text-2xl pb-2">Credits</h1>
            <h2 class="text-xl"><a class="text-blue-700 hover:text-blue-300" href="https://threadreaderapp.com">Thread Reader</a></h2>
            <h2 class="text-xl"><a class="text-blue-700 hover:text-blue-300" href="https://github.com/abraham/twitter-status">Twitter Status</a></h2>
            <p>Twitter Status is Javascript Based Web Component which helps build the Twitter Status UI.</p>
            <p>Developer: <a class="text-blue-700 hover:text-blue-300" href="https://twitter.com/abraham">Abraham</a></p>
        </div>
        <div class="pb-4" id="about-support-us">
            <h1 class="text-2xl pb-2">Support Us</h1>
            <p>Thank you for using our service and stopping by.</p>
            <p>Please continue using the service and report issues at {{config('unquote.email')}} if you find any.</p>
        </div>
    </div>
</div>
@endsection
