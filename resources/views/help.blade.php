@extends('layouts.app')

@section('title', 'Help')

@section('content')
    <div><h1 class="text-4xl py-4 pl-4">Help</h1></div>
    <div class="pb-4"><hr /></div>
    <div class="flex flex-col md:flex-row">
        <div class="flex flex-col mb-4">
            <ul class="list-disc border border-2 border-gray-500 rounded-lg mx-4 px-8">
                <li class="py-2"><a href="#faq-what-is-unchain">What is Unchain?</a></li>
                <li class="py-2"><a href="#faq-how-to-unchain">How to unchain quotes?</a></li>
                <li class="py-2"><a href="#faq-bot-responses">Bot responses</a></li>
                {{-- <li class="py-2"><a href="#faq-privacy-policy">Privacy Policy</a></li> --}}
                <li class="py-2"><a href="#faq-why-free">Why Free?</a></li>
                <li class="py-2"><a href="#faq-paid-features">What are the paid features?</a></li>
                {{-- <li class="py-2"><a href="#faq-6">Edit already unchained quotes</a></li> --}}
                <li class="py-2"><a href="#faq-delete-tweets">Want to delete my tweet(s)</a></li>
                <li class="py-2"><a href="#faq-bug-reporting">There is a bug I want to report</a></li>
            </ul>
        </div>
        <div class="flex flex-col px-4">
            <div class="pb-4" id="faq-what-is-unchain">
                <h1 class="text-2xl">What is Unchain?</h1>
                <p>Twitter allows user to Quote(Reply with Comments) other people. </p>
                <p>A Quote can have a quote and so on... these are chained tweets</p>
                <p>If there are many tweets in the chain it is diffult for a user to get the context until they reach the end</p>
                <p>This bot will help users to view the conversation in the single page, so Unchain.</p>
            </div>
            <div class="pb-4" id="faq-how-to-unchain">
                <h1 class="text-2xl">How to unchain quotes?</h1>
                <ol class="list-decimal pl-5">
                    <li>Follow {{'@'.config('unquote.screen_name')}} so you can find the bot easily.</li>
                    <li>Reply the tweet you want to unchain and just mention this <code>{{'@'.config('unquote.screen_name')}} {{config('unquote.mention_placeholder')}}</code> in your tweet.</li>
                </ol>
            </div>
            <div class="pb-4" id="faq-bot-responses">
                <h1 class="text-2xl">Bot responses</h1>
                <ul class="list-disc pl-8">
                    {{-- <li>{{'@'.config('unquote.screen_name')}} is not authorized to view the private tweets.</li> --}}
                    <li>Chain is not long enough. Chain should consist of at least 3 tweets.</li>
                    <li>Something went wrong.</li>
                </ul>
            </div>
            {{-- <div class="pb-4" id="faq-privacy-policy">
                <h1 class="text-2xl">Privacy Policy</h1>
                <p>You own your content. Check our <a class="text-blue-500 hover:text-blue-300" href="{{url('/privacy_policy')}}">Privacy Policy</a> for more details.</p>
            </div> --}}
            <div class="pb-4" id="faq-why-free">
                <h1 class="text-2xl">Why Free?</h1>
                <p>No feature currently is worth the money, honestly.</p>
            </div>
            <div class="pb-4" id="faq-paid-features">
                <h1 class="text-2xl">What are the paid features?</h1>
                <p>Will plan something useful based on feedback.</p>
                <p>Please contact {{config('unquote.email')}} if you have any suggestions or feedback.</p>
                <p>Custom features are also welcome.</p>
            </div>
            {{-- <div class="py-4" id="faq-6">
                <h1 class="text-2xl">Edit already unchained quotes</h1>
            </div> --}}
            <div class="pb-4" id="faq-delete-tweets">
                <h1 class="text-2xl">I want to delete my tweet(s)</h1>
                <p>Please contact us at {{config('unquote.email')}} with your Twitter Handle.</p>
                <p>We will remove all the existing and future tweets from our website.</p>
            </div>
            <div class="pb-4" id="faq-bug-reporting">
                <h1 class="text-2xl">There is a bug I want to report.</h1>
                <p>Yes, there is a possibility as we are still learning many things from this.</p>
                <p>Please contact us at {{config('unquote.email')}} so we can work on that fix.</p>
            </div>
        </div>
    </div>
@endsection
