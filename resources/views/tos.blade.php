@extends('layouts.app')

@section('title', 'ToS')

@section('content')
<div><h1 class="text-4xl py-4">Terms of Service</h1></div>
<div class="pb-4"><hr /></div>
<div>
    <p>
    By using Unchain Quotes (the "Site"), you agree to comply with and be bound by these Terms of Use (the "Unchain Quotes") and/or any revised version. Any new service available on the Site or any modification of an existing service will be governed by the TOS, which may be modified or updated from time to time. We strongly encourage you to review the TOS regularly. The Site is operated in Mumbai, MH, India.
    </p>
</div>

<div><h1 class="text-3xl py-2">Provided Service</h1></div>
<div>
    <p>
    Unchain Quotes makes available to its users ("You") access to a fully customizable Site that allows you to select and aggregate content provided by third parties ("Third Party Content") through computer programs called unchainer ("Unchainer (s)", and tools that allow you to create and publish any content, of any kind, in accordance with the terms of the Unchain Quotes Terms and Conditions ("Unchain Quotes"). Unchain Quotes does not provide content but rather functions as an aggregator. An Unchainer is defined as software that provides content, including data from third parties. It is composed of computer code, which is the exclusive property of Unchain Quotes.
    <br/><br/>
    Use of the Service is free of charge. However, access to Third Party Content may be subject to a charge: this billing is done outside of Unchain Quotes. Unchain Quotes does not host any Third Party Content and retains only technical and personal data in accordance with our privacy policy.
    </p>
</div>

<div><h1 class="text-3xl py-2">Conduct</h1></div>

<div><h1 class="text-2xl py-2">Conduct of Users</h1></div>
<div>
    <p>
        As a condition of accessing and using the Site and the Service, you agree to: Not to post, share or transmit in any way any illegal, obscene, harmful, harmful, threatening, defamatory, hateful or hateful content or content that contains objects or symbols of hatred, invades the privacy of any third party or is otherwise objectionable.
        <br><br>
        Respect the privacy of others and the confidentiality of information you may receive and not post, email or otherwise transmit unsolicited or unauthorized advertising, promotional material, spam, chain letters, pyramids or any other form of solicitation.
        <br><br>
        Do not harass another user of the Site or impersonate another user by any means. Always provide truthful, truthful and up-to-date personal data and information and update such data and information as necessary to keep it true, complete, accurate and up-to-date.
        <br><br>
        Respect the rights of others and, more generally, all applicable laws and regulations relating to your use of the Service, and not make available any content that you do not have the legal right to transmit (including any content that violates any confidentiality or fiduciary obligation you may have) that infringes the intellectual property rights of any third party (including, but not limited to, copyrights, trademarks, trade-marks, trademarks, etc.). Not to engage in conduct, display or share any content that interrupts, destroys, restricts or otherwise adversely affects the Site and/or the Service or allows you to access the Site without permission, including by using viruses, malicious computer codes, programs or files.
        <br><br>
        Not to reproduce, copy, sell or use commercially (including the right of access) in whole or in part, the Service and/or the Site.
        <br><br>
        In the event of any violation of the foregoing, Unchain Quotes reserves the right to block your access to the Site and to disclose any information necessary to satisfy any law, regulation, governmental request or partner request, in its sole discretion. Unchain Quotes also reserves the right to block the distribution of Your Content or any Third Party Content through the Service.
    </p>
</div>


<div><h1 class="text-2xl py-2">Important Information</h1></div>
<div>
    <p>
        To access and use the Service, you must be legally able to enter into a binding agreement and acknowledge
        that certain Third Party Content may be considered offensive by some and not suitable for persons under
        the age of 18. You must also comply with all applicable laws and regulations in India and, if applicable, your country of origin.
        <br><br>
        You also acknowledge that the activity of creating, submitting or sharing Your Content may give rise to
        various types of legal liability and you represent that Your Content (whether or not You are the author of
        such Content) is at all times (whether or not You are the author of such Content) (both at the time of its
        initial submission and throughout its access to the Site) compliant with the TOS and all applicable laws.
        You understand that Unchain Quotes has no control (other than technical if necessary) and
        does not pre-select Your Third Party Content and cannot be held responsible for their communication to the public.
    </p>
</div>


<div><h1 class="text-3xl py-2">Posting and Sharing Content</h1></div>

<div><h1 class="text-2xl py-2">Basic Principles</h1></div>

<div>
    <p>
        You can publish and share Your Content by sending Third Party Content to a Unchainer. You acknowledge that you share Your Content under your sole responsibility and declare that you own all rights or have obtained the necessary permissions for such Content. You must verify that Your Content is compliant with the TOU and, more generally, that it is not harmful or illegal. You agree to comply with all Unchain Quotes policies or technical requirements, to provide all data and information, whether personal or otherwise, requested by Unchain Quotes.
        <br><br>
        You understand that you cannot request any payment from Unchain Quotes to view or share unchained content.
    </p>
</div>

<div><h1 class="text-2xl py-2">Distribution of Your Content by Unchain Quotes</h1></div>
<div>
    <p>
        You expressly grant Unchain Quotes the right, directly and indirectly, to distribute the content of Your Third-Party Chain. Unchain Quotes may distribute Your Content for commercial and non-commercial purposes without being liable for any payment or compensation of any kind by Unchain Quotes to you. Unchain Quotes will, in its sole discretion, decide whether or not (and how) it will distribute Your Content, choose its business partners, and determine all terms and conditions of such distribution. If you do not agree with the above, you should not use Unchainer.
    </p>
</div>

<div><h1 class="text-2xl py-2">Identification of Your Content</h1></div>
<div>
    <p>
        As we have no control over Your Content, it is your sole responsibility when posting Your Content on Unchain Quotes to clearly and truly identify its actual or potential nature in order to ensure its classification into the appropriate category. This is necessary for the proper functioning of the Service and to protect other users by identifying content that may be explicit or pornographic.
    </p>
</div>

<div><h1 class="text-3xl py-2">Disclaimer</h1></div>

<div><h1 class="text-2xl py-2">Limitation of Liability</h1></div>
<div>
    <p>
        The Service and the Site are made available to you "as is". Unchain Quotes makes no warranties, whether express or implied, with respect to the Service and the Site. In particular, and without limitation, we cannot guarantee the continuity and quality of the Service available on this Site, nor can we guarantee that the Site will be accessible at all times, or that our users will not violate the TOS or our privacy policy. We may limit the use of our Service. We cannot be held responsible for the impossibility of making available or removing content or messages exchanged between users.
        <br><br>
        You understand that you access and use the Site and the Service at your own risk. Unchain Quotes is not responsible for your reliance on any information or data that you have accessed, directly or indirectly, through the Service, nor for your access to or downloading of any material, program or file through the Service. Unchain Quotes cannot be held responsible for any loss of data or damage to your computer system or any other device used to access the Service.
        <br><br>
        Although we make every effort to ensure the confidentiality of your personal data, we shall not be liable for any breach of such confidentiality, regardless of the cause (including but not limited to technical difficulty, intrusion into our computer system or human error).
    </p>
</div>

<div><h1 class="text-2xl py-2">Third-Party Content - Links</h1></div>
<div>
    <p>
        Unchain Quotes has no control over and does not host Third Party Content or services that may be provided by third parties through the Chains, or the Site (the "Third Party Services"). Similarly, we cannot control links to other websites or Internet resources. These resources, content and services are provided to you "as is" to facilitate your browsing on the Web. As such, you acknowledge that we cannot be held responsible for the resources, content or services of third parties and you agree to communicate directly and exclusively with the third party concerned for any claim or request you may have. We are not responsible for the content, advertising, products, services or any other data or information available on or from these external sites or sources. In addition, you understand that we cannot be held responsible for any damage or loss arising from the use of the content, goods or services available on these external sites or sources or related to their use or reliance on them.
    </p>
</div>

<div><h1 class="text-3xl py-2">Proprietary Rights</h1></div>

<div><h1 class="text-2xl py-2">Access to the Site</h1></div>
<div>
    <p>
        Unchain Quotes is the sole owner of all proprietary rights in the Site and the Service, including the users' databases. Any unauthorized reproduction, display, publication, transmission or, more generally, any unauthorized use of the Site or the Service may incur your liability and may result in criminal or civil proceedings. You agree not to access the Service by any means other than those made available to you by Unchain Quotes.
        <br><br>
        The use of Third Party Content may only be made in accordance with the authorization or license granted by the respective rights holders.
    </p>
</div>

<div><h1 class="text-2xl py-2">Intellectual Property</h1></div>
<div>
    <p>
        The Site is an original work protected by intellectual property laws and international conventions. Its structure and available Third Party Content may be protected by copyright or other intellectual property rights.
        <br><br>
        You warrant that you will not modify, rent, borrow, sell or distribute such works or create derivative works based in whole or in part on the Site or Third Party Content. You may access the Site for your personal and private use. Printing is limited to your personal, non-commercial use only; any other use or communication is prohibited without the prior written permission of Unchain Quotes. All rights not expressly granted to you by the TOU are reserved.
    </p>
</div>

<div><h1 class="text-2xl py-2">Intellectual Property of Others</h1></div>
<div>
    <p>
        We respect the intellectual property of others. If you believe that the material on which you have rights has been reproduced or used in such a way as to constitute an infringement, please contact us with your personal information, a description of your work, the location on the Site of potentially infringing material, a statement of the rights you have claimed on the work, and a statement of the lack of authorization given to the party using the material.
    </p>
</div>

<div><h1 class="text-3xl py-2">Third Party Rights</h1></div>
<div>
    <p>
        Without prejudice to the extent that Unchain Quotes may take to protect its rights, Unchain Quotes reserves the right, without justification or prior notice, to prevent access to any Content or to remove or deactivate any unchain that may adversely affect the rights of third parties, which you expressly agree.
    </p>
</div>


<div><h1 class="text-3xl py-2">Warranties</h1></div>
<div>
    <p>
        You expressly warrant that you are the owner of all rights to use and distribute Your Content or that you have obtained all necessary authorizations for the use and distribution of Your Content, including the right to sub-license such rights. As Unchain Quotes has no control over Your Content, you understand that Unchain Quotes cannot be held responsible for its communication to the public and share Your Content under your sole responsibility.
        <br><br>
        Your Content must at all times comply with the Unchain Quotes TOS and, more generally, must not be harmful or unlawful, in particular to minors.
    </p>
</div>

<div><h1 class="text-3xl py-2">Twitter specifics</h1></div>
<div>
    <p>
        In Twitter <a class="text-blue-700 hover:text-blue-300" href="https://twitter.com/en/tos#us">own term of service</a> it is explicitly stated: "You retain your rights to any Content you submit, post or display on or through the Services. What’s yours is yours — you own your Content (and your photos and videos are part of the Content)."
        And so you do on Unchain Quotes too.
        <br><br>
        However, you also grant them some rights: "By submitting, posting or displaying Content on or through the Services, you grant us a worldwide, non-exclusive, royalty-free license (with the right to sublicense) to use, copy, reproduce, process, adapt, modify, publish, transmit, display and distribute such Content in any and all media or distribution methods (now known or later developed). This license authorizes us to make your Content available to the rest of the world and to let others do the same."
        <br><br>
        Unchain Quotes is bound to Twitter by the Developer Agreement and Policy that grant us a Licence to display your content.
        <br><br>
        If you want your content removed from Unchain Quotes please read our help page <a class="text-blue-700 hover:text-blue-300" href="{{url('/help')}}">here</a>.
    </p>
</div>
<br><br>
<div>
    <p>
        Last update: March 2020
    </p>
</div>


@endsection
