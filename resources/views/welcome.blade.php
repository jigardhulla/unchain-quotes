@extends('layouts.app')

@section('title', 'Welcome')

@section('content')
    <div class="w-full flex flex-col justify-center text-center content-center h-screen">
        <div class="text-4xl">Take a snapshot of your favorite Conversations on Twitter</div>
        <div>Mention <code class="bg-gray-700 text-white px-2 my-5 rounded">{{'@' . config('unquote.screen_name')}} {{config('unquote.mention_placeholder')}}</code> as a Reply or as a Quote</div>
    </div>
@endsection
