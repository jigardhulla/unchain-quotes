<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkTweet extends Model
{
    public $fillable = [
        'link_id',
        'tweet_id'
    ];
}
