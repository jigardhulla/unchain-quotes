<?php

namespace App;

use Log;

class TweetStatusGenerator {

    const DELIMETER = ': ';
    const DELIMETER_FOCUS = ', ';
    const MENTION_PREFIX = '@';
    const CHAR_LIMIT = 280;
    const SUFFIX_LAST_MESSAGE = '... ';
    const MAX_MENTIONS_LENGTH = 3;

    public $linkUrl, $mention, $welcome, $focus, $lastStatus;

    protected $welcomeMessages = [
        'Hello',
        'Namaste',
        'Bonjour', // French
        'Hola', // Spanish
        'Zdravstvuyte', // Russian
        'Nǐn hǎo', // Chinese
        'Salve', // Italian
    ];

    protected $focusMessages = [
        'please find the conversation',
        'here is your conversation',
        'the conversation you asked for',
    ];

    private function _randomizer(array $array){
        return $array[rand(0, count($array) - 1)];
    }

    public function __construct(){
        
    }

    public function setLastStatus($lastStatus){
        $this->lastStatus = $lastStatus;
    }

    public function setWelcomeMessage(){
        $this->welcome =  $this->_randomizer($this->welcomeMessages) . self::DELIMETER_FOCUS;
    }

    public function setFocusMessage(){
        $this->focus = $this->_randomizer($this->focusMessages) . self::DELIMETER;
    }

    public function setMention(array $mentions){
        $mentions = array_unique($mentions);
        $mentions = array_splice($mentions, 0, self::MAX_MENTIONS_LENGTH);
        $mentions = self::MENTION_PREFIX . implode(' ' . self::MENTION_PREFIX, $mentions);

        $this->mention = $mentions . self::DELIMETER;
    }

    public function setLinkUrl($linkUrl){
        $this->linkUrl = $linkUrl;
    }
    
    public function generateTweetStatus() : string {
        $remaining = $this->spaceRemaining();
        $trimmedLastStatus = $this->trimLastStatus($remaining);
        Log::info(__METHOD__ . " trimmedLastStatus: " . $trimmedLastStatus);
        $message = implode([
            $this->welcome,
            $this->focus,
            $this->mention,
            $trimmedLastStatus,
            $this->linkUrl
        ]);
        Log::info(__METHOD__ . " Message: " . $message);
        return $message;
    }

    protected function spaceRemaining() : int {
        return self::CHAR_LIMIT 
            - \strlen($this->welcome) 
            - \strlen($this->focus) 
            - \strlen($this->mention)
            - \strlen($this->linkUrl)
            - \strlen(self::SUFFIX_LAST_MESSAGE);
    }

    protected function trimLastStatus($remaining){
        return \substr($this->lastStatus, 0, $remaining) . self::SUFFIX_LAST_MESSAGE;
    }
}