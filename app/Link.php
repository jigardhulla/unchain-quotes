<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Link extends Model
{
    use HasSlug;

    const SLUG_LENGTH = 50;

    public $fillable = ['tweeter_id', 'first_tweet_id'];

    public function mentionRequests(){
        return $this->hasMany(MentionRequest::class);
    }

    public function tweets(){
        return $this->belongsToMany(Tweet::class, 'link_tweets');
    }

    public function firstTweet(){
        return $this->hasOne(Tweet::class, 'id', 'first_tweet_id');
    }

    public function lastTweet(){
        return $this->hasOne(Tweet::class, 'id', 'last_tweet_id');
    }
    
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(function($link){
                $firstTweet = $link->firstTweet;
                return $firstTweet->text;
            })
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(self::SLUG_LENGTH);
    }
    
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
