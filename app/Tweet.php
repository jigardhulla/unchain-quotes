<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{

    public $fillable = [
        'status_id',
        'tweeter_id',
        'text',
        'tweet_created_at',
        'is_request',
        'full_tweet',
    ];

    public $dates = [
        'tweet_created_at',
    ];

    // public function links(){
    //     return $this->belongsToMany(Link::class)->using(MentionRequest::class)->withTimestamps();
    // }

    public function menionRequest(){
        return $this->hasOne(MentionRequest::class);
    }

    public function links(){
        return $this->belongsToMany(Link::class, 'link_tweets');
    }

    public function tweeter(){
        return $this->belongsTo(Tweeter::class);
    }

    public function getTweetObject(){
        return json_decode($this->full_tweet);
    }

    public function getEntityUrls(){
        $tweetObj = $this->getTweetObject();
        return $tweetObj->entities->urls;
    }

    public function getEntityMedia(){
        $media = [];
        $tweetObj = $this->getTweetObject();
        if(\property_exists($tweetObj, 'extended_entities') && property_exists($tweetObj->extended_entities, 'media')){
            $media = $tweetObj->extended_entities->media;
        }
        return $media;
    }

    public function getEntityHashtags(){
        $tweetObj = $this->getTweetObject();
        return $tweetObj->entities->hashtags;
    }

    public function getEntityUserMentions(){
        $tweetObj = $this->getTweetObject();
        return $tweetObj->entities->user_mentions;
    }

    public function getAllMediaByType(): array{
        $all = [];
        $media = $this->getEntityMedia();
        foreach($media as $content){
            $all[$content->type] = $media;
        }
        return $all;
    }

    public function getPhotos(): array{
        $media = $this->getAllMediaByType();
        return $media['photo'] ?? [];
    }
}
