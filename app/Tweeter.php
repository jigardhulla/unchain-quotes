<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweeter extends Model
{
    public $fillable = [
        'user_id',
        'name',
        'screen_name',
        'verified',
        'profile_image_url'
    ];
}
