<?php

namespace App\Service\TwitterService;

use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Support\Facades\Log;

class TwitterService {

    const LINK_LENGTH = 23;
    const TWEET_LENGTH = 280;

    protected $conn = null;

    protected $defaultGetMentionsParams = [
        'include_entities' => 0,
        'tweet_mode' => 'extended',
    ];

    // TODO: Config from .env
    public function __construct()
    {
        // $this->conn = new TwitterOAuth(
        //     'pNgCAWyT7ZGx5PqvNySAI48Md',
        //     'D1OVAXQKPuH6rbawozLHPi7NaHri8CtXAMqevojHrjqHx5tirb',
        //     '84839729-XamUKmFQCx2eB8VRyqV1SzvlTTDuniZz8XQtQj3RX',
        //     'hqQaowuab8hPig3GTlBzu1Fm1dUbwVf8OCcBXVabrOAV5'
        // );
        $this->conn = new TwitterOAuth(
            config('unquote.twitter_api_key'),
            config('unquote.twitter_api_secret_key'),
            config('unquote.twitter_access_token'),
            config('unquote.twitter_token_secret')
        );
    }

    /**
     * Get Mentions List
     */
    public function getMentions($limit, $lastStatusId=null){
        $params = array_merge($this->defaultGetMentionsParams, ['count' => $limit]);

        if($lastStatusId){
            $params = array_merge($params, [
                'since_id' => $lastStatusId,
            ]);
        }

        Log::debug("Twitter API Params: ", $params);
        $response = $this->conn->get("statuses/mentions_timeline", $params);
        Log::debug("Twitter API Response: ", $response);
        // $this->_isValidResponse($response);
        return $response;
    }

    // TODO: Response Validation
    // private function _isValidResponse($response){
    //     if(!empty($response)){
    //         return true;
    //     }
    //     throw new TwitterServiceException(500, 'Invalid Response');
    // }

    public function getStatus($statusId){
        $response = $this->conn->get('statuses/show', ["id" => $statusId, "tweet_mode" => "extended"]);
        //$this->_isValidResponse($response);
        return $response;
    }

    public function reply($message, $replyToStatusId){
        Log::info("In Reply to Status ID: " . $replyToStatusId);
        return $this->create($message, ['in_reply_to_status_id' => $replyToStatusId]);
    }

    public function create(string $message, array $params = []) {
        $response = $this->conn->post('statuses/update', array_merge(["status" => $message], $params));
        //$this->_isValidResponse($response);
        return $response;
    }

    public static function isTweetLengthValid(string $tweetStatus) : bool {
        Log::info("Tweet length: " . \strlen($tweetStatus));
        return \strlen($tweetStatus) <= self::TWEET_LENGTH ? true : false;
    }
}
