<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MentionRequest extends Model
{
    public $fillable = ['link_id', 'tweet_id'];

    public function link(){
        return $this->belongsTo(Link::class);
    }
}
