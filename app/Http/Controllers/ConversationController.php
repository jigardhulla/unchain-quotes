<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Link;

class ConversationController extends Controller{
    public function legacyShow(Link $link){
        // $link = Link::with('tweets.tweeter')->find($id)->toArray();
        // $tweets = array_reverse($link['tweets']);
        $tweets = array_reverse($link->tweets->toArray());
        return view('unquote', ['tweets' => $tweets, 'tweetCount' => count($tweets)]);
    }

    public function show(Link $link){
        $tweets = $link->tweets->reverse();
        $media = $this->getMedia($tweets);
        return view('conversation', [
            'tweets' => $tweets, 
            'tweetCount' => count($tweets),
            'media' => $media,
        ]);
    }

    public function getMedia($tweets){
        $mediaContent = [];
        foreach($tweets as $tweet){
            $mediaContent[$tweet->status_id] = [];
            $medias = $tweet->getAllMediaByType();
            foreach($medias as $key => $media){
                $mediaContent[$tweet->status_id][$key] = $media;
            }
        }
        return $mediaContent;
    }
}

?>
