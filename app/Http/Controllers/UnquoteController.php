<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Link;

class UnquoteController extends Controller{
    public function show($id){
        $link = Link::with('tweets.tweeter')->find($id)->toArray();
        $tweets = array_reverse($link['tweets']);
        return view('unquote', ['tweets' => $tweets, 'tweetCount' => count($tweets)]);
    }
}

?>
