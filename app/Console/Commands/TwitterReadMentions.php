<?php

namespace App\Console\Commands;

use App\Jobs\ReplyTweet;
use App\Jobs\UnquoteTweet;
use App\Service\TwitterService\TwitterService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class TwitterReadMentions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:read_mentions
                            {--limit=200                : Limit to fetch latest mentions }
                            {--screen-name=QuotesUnchain : Screen Name of the bot to remove the screen name from the mentioned tweet }
                            {--place-holder=unchain     : String to look for in the mention }
                            {last_status_id?            : Status ID last processed }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Mentions(Latest) from Twitter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(TwitterService $twitter)
    {
        try{
            $lastStatusId = Storage::exists('last_status_id') ? Storage::get('last_status_id') : null;
            //$lastStatusId = null;

            $limit = $this->option('limit');

            $this->info("Fetching Mentions...");
            $tweets = $twitter->getMentions($limit, $lastStatusId);

            if(empty($tweets)){
                $this->info("No mentions to process");
                exit(0);
            }

            $this->info("Reverse the tweets, first mention should be processed first");
            $tweets = array_reverse($tweets);

            $this->info("Loop through each mention...");
            foreach($tweets as $tweet){
                // $this->info("Tweet:" . json_encode($tweet, JSON_PRETTY_PRINT));
                if($this->_containsPlaceHolder($tweet) && (!empty($tweet->in_reply_to_status_id_str) || !empty($tweet->is_quote_status))){
                    Log::info("Processing Tweeet: " . $tweet->id_str);
                    $this->info("Dispatch a Job to Unquote the Tweet");
                    UnquoteTweet::withChain([
                        new ReplyTweet($tweet),
                    ])->dispatch($tweet);
                }else{
                    $this->info("Dispatch a job to reply with standard response");
                    // @TODO: Standard Response
                    //ReplyTweet::dispatch($tweet->id_str);
                }
                $lastStatusId = $tweet->id_str;
            }

            Storage::put('last_status_id', $lastStatusId);
            $this->info("Recorded last status id");


        }catch(\Exception $e){
            Log::error("Something went wrong in method " . __METHOD__ . ". Error: " . $e->getMessage(), $e->getTrace());
            $this->error("Oops, something went wrong! " . "Error: " . $e->getMessage());
        }
    }

    private function _containsPlaceHolder($tweet){
        $placeHolder = $this->hasOption('place-holder') ? $this->option('place-holder') : config('unquote.mention_placeholder');
        $screenName = $this->option('screen-name');
        $text = isset($tweet->text) ? $tweet->text : $tweet->full_text;

        $trimmedText = str_replace('@'.$screenName, '', $text);

        $this->info("Looking for placeholder in text: " . $trimmedText);
        $check = preg_match('/'.$placeHolder.'/i', $trimmedText);

        $this->info("Placeholder exists: " . $check);
        return $check;
    }
}
