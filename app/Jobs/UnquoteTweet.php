<?php

namespace App\Jobs;

use App\Exceptions\UnquoteTweetException;
use App\Link;
use App\LinkTweet;
use App\MentionRequest;
use App\Service\TwitterService\TwitterService;
use App\Tweet;
use App\Tweeter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class UnquoteTweet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mention;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tweet)
    {
        $this->mention = $tweet;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TwitterService $twitterService)
    {
        $this->twitterService = $twitterService;
        // Store Mentioner Details
        $user = $this->mention->user;
        $tweeter = Tweeter::firstOrCreate(
            ['user_id' => $user->id],
            [
                'name' => $user->name,
                'screen_name' => $user->screen_name,
                'profile_image_url' => $user->profile_image_url_https,
                'verified' => $user->verified,
            ]
        );

        $mentionedTweet = Tweet::firstOrCreate(
            ['status_id' => $this->mention->id],
            [
                'text' => isset($this->mention->text) ? $this->mention->text : $this->mention->full_text,
                'tweet_created_at' => date('Y-m-d H:i:s', strtotime($this->mention->created_at)),
                'tweeter_id' => $tweeter->id,
                'is_request' => 1,
                'full_tweet' => json_encode($this->mention),
            ]
        );

        // 1. Get Tweet ID to be unquoted
        $parentRootTweetId = $this->getParentId($this->mention);
        try{
            //DB::beginTransaction();

            // 3. Get the tweet from that ID
            // 4. Store the Tweeter & Tweet Details
            // 5. Get the next Tweet ID and repeat from 2
            $this->storeStatusAndGetNextId($mentionedTweet, $parentRootTweetId);
            //DB::commit();
        }catch(UnquoteTweetException $ute){
            // DB::rollback();
            Log::info($ute->getMessage());
        }
    }

    // TODO: Simplify this function
    function storeStatusAndGetNextId($mentionedTweet, $statusId, $link = null, $childTweet = null){

        $content = $this->twitterService->getStatus($statusId);
        if(isset($content->errors) && !empty($content->errors)){
            throw new UnquoteTweetException($content->errors[0]->message);
        }

        if(!empty($content) && !isset($content->errors)){

            $user = $content->user;
            $tweeter = Tweeter::firstOrCreate(
                ['user_id' => $user->id],
                [
                    'name' => $user->name,
                    'screen_name' => $user->screen_name,
                    'profile_image_url' => $user->profile_image_url_https,
                    'verified' => $user->verified,
                ]
            );

            $tweet = Tweet::firstOrCreate(
                ['status_id' => $content->id],
                [
                    'text' => isset($content->text) ? $content->text : $content->full_text,
                    'tweet_created_at' => date('Y-m-d H:i:s', strtotime($content->created_at)),
                    'tweeter_id' => $tweeter->id,
                    'full_tweet' => json_encode($content),
                ]
            );

            if($childTweet != null){
                $childTweet->parent_tweet_id = $tweet->id;
                $childTweet->update();
            }


            if($link == null){
                $link = Link::firstOrCreate(['first_tweet_id' => $tweet->id]);
                $linkMentions = MentionRequest::firstOrCreate(['link_id' => $link->id, 'tweet_id' => $mentionedTweet->id]);
                if(!$link->wasRecentlyCreated){
                    throw new UnquoteTweetException("Link Exists", 1);
                }

            }

            $chain = LinkTweet::create(['link_id' => $link->id, 'tweet_id' => $tweet->id]);

            if($content->is_quote_status && isset($content->quoted_status->id_str)){
                $this->storeStatusAndGetNextId($mentionedTweet, $content->quoted_status->id_str, $link, $tweet);
            }else{
                $link->last_tweet_id = $tweet->id;
                $link->save();
            }
        }
    }

    public function getParentId($mention){
        if($mention->is_quote_status){
            return $mention->quoted_status->id_str;
        }

        return $mention->in_reply_to_status_id;
    }
}
