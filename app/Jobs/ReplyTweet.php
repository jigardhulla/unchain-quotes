<?php

namespace App\Jobs;

use App\Service\TwitterService\TwitterService;
use App\Tweet;
use App\TweetStatusGenerator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ReplyTweet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mention;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tweet)
    {
        $this->mention = $tweet;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TwitterService $twitterService)
    {
        $this->twitterService = $twitterService;
        Log::info("Mention Tweet in Reply Tweet " . json_encode($this->mention));

        // Get Unquoted Link URL
        // We will reply to this tweet id
        $mentionedTweet = Tweet::with('tweeter')->where('status_id', $this->mention->id_str)->first();

        // Get Mentioner Handler
        // We need to at least mention the requester in the response
        $twitterHandles[] = $mentionedTweet->tweeter->screen_name;

        // Get Link to all tweets in the conversation
        $link = $mentionedTweet->menionRequest->link;

        // Get last tweet
        // Will include glimpse of how conversation started in our response
        $lastTweet = $link->lastTweet;

        // Get all tweets to get the handlers involved in the conversation
        $tweets = $link->tweets;
        foreach($tweets as $tweet){
            $twitterHandles[] = $tweet->tweeter->screen_name;
        }

        // URL to the readable conversation
        $linkUrl = route('conversation', ['link' => $link->slug]);

        // Generate message
        $message = $this->generateMessage($lastTweet->text, $twitterHandles, $linkUrl);
        Log::info("Message: " . $message);

        // Check the length before making API Call
        if(!TwitterService::isTweetLengthValid($message)){
            throw new \Exception("Invalid length for tweet");
        }

        // Reply to Mentioner
        $repliedTweet = $this->twitterService->reply($message, $mentionedTweet->status_id);
        Log::info("Twitter Response: " . json_encode($repliedTweet));
        
    }

    public function generateMessage(string $lastStatus, array $mentions, string $linkUrl) : string{
        $tweetStatusGenerator = new TweetStatusGenerator();
        $tweetStatusGenerator->setLastStatus($lastStatus);
        $tweetStatusGenerator->setLinkUrl($linkUrl);
        $tweetStatusGenerator->setMention($mentions);
        $tweetStatusGenerator->setWelcomeMessage();
        $tweetStatusGenerator->setFocusMessage();

        $message = $tweetStatusGenerator->generateTweetStatus();
        return $message;
    }
}
