<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/pricing', function () {
    return view('pricing');
});

Route::get('/help', function () {
    return view('help');
});

Route::get('/tos', function () {
    return view('tos');
});

Route::get('/privacy_policy', function () {
    return view('privacy_policy');
});

Route::get('/unquote/{id}', 'UnquoteController@show')->name('unquote');
Route::get('/conversation-legacy/{link}', 'ConversationController@legacyShow')->name('conversation-legacy');
Route::get('/conversation/{link}', 'ConversationController@show')->name('conversation');
