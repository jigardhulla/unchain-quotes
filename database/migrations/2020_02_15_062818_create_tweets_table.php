<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('tweeter_id');
            $table->unsignedBigInteger('parent_tweet_id')->nullable();
            $table->text('text', 300);
            $table->timestamp('tweet_created_at');
            $table->boolean('is_request')->default(0);
            $table->json('full_tweet');
            $table->timestamps();

            $table->foreign('tweeter_id')->references('id')->on('tweeters');
            $table->foreign('parent_tweet_id')->references('id')->on('tweets');
            $table->index("status_id");
            $table->index("tweet_created_at");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweets');
    }
}
