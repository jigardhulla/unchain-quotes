FROM php:7.2-apache

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

COPY . /var/www/html

ENV APACHE_DOCUMENT_ROOT /var/www/html/unchain-quotes/public

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN apt-get update \
    && apt-get install zip unzip \
    && docker-php-ext-install mysqli pdo pdo_mysql \
    && a2enmod rewrite

ARG APP_VERSION
ENV APP_VERSION ${APP_VERSION}

WORKDIR /var/www/html/unchain-quotes